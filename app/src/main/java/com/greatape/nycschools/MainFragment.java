package com.greatape.nycschools;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.greatape.engine.NYCSchoolEngine;
import com.greatape.engine.gson.NYCSchool;
import com.greatape.nycschools.adapter.SchoolListAdapter;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class MainFragment extends ListFragment {
    private final static String TAG = "MainFragment";

    private SchoolListAdapter mAdapter;
    private OnSchoolSelectedListener mOnSchoolSelectedListener;

    public interface OnSchoolSelectedListener {
        void onSchoolSelected(String dbn);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAdapter = new SchoolListAdapter(getContext());
        setListAdapter(mAdapter);

        loadSchoolList();
    }

    private void loadSchoolList() {
        NYCSchoolEngine engine = NYCSchoolEngine.getInstance();
        engine.getSchoolList(new Observer<List<NYCSchool>>() {
            @Override
            public void onSubscribe(Disposable d) {}

            @Override
            public void onNext(List<NYCSchool> nycSchools) {
                mAdapter.addAll(nycSchools);
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "Error retrieving school list: " + e.toString());
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle(R.string.school_list_error);
                builder.setMessage(e.getMessage());
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }

            @Override
            public void onComplete() {
                Log.d(TAG, "List complete");
            }
        });
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity = getActivity();
        if (activity instanceof OnSchoolSelectedListener) {
            mOnSchoolSelectedListener = (OnSchoolSelectedListener)activity;
        } else {
            Log.e(TAG, "Error getActivity() does not return an instance of OnSchoolSelectedListener");
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        NYCSchool school = mAdapter.getItem(position);
        if (school != null) {
            mOnSchoolSelectedListener.onSchoolSelected(school.dbn);
        }
    }
}
