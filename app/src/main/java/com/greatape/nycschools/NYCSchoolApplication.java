package com.greatape.nycschools;

import android.app.Application;

import com.greatape.engine.NYCSchoolEngine;

public class NYCSchoolApplication extends Application {
    // TODO: If we are going to open source this code this token would need to loaded
    // from a configuration file that could be excluded from the public repo.
    private final static String APP_TOKEN = "Blvbt7s80EiMNTOPtaCsL9dNn";

    public void onCreate() {
        super.onCreate();
        NYCSchoolEngine.getInstance().setAppToken(APP_TOKEN);
    }
}
