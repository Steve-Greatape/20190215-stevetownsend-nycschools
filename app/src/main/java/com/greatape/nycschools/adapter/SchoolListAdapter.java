package com.greatape.nycschools.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.greatape.engine.gson.NYCSchool;
import com.greatape.nycschools.R;

public class SchoolListAdapter extends ArrayAdapter<NYCSchool> {

    public SchoolListAdapter(@NonNull Context context) {
        super(context, R.layout.school_item, R.id.school_name);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.school_item, parent, false);
        }
        NYCSchool school = getItem(position);
        if (school != null) {
            TextView name = convertView.findViewById(R.id.school_name);
            name.setText(school.school_name);
            TextView borough = convertView.findViewById(R.id.school_borough);
            borough.setText(school.borough);
        }
        return convertView;
    }
}
