package com.greatape.nycschools;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements MainFragment.OnSchoolSelectedListener {
    // TODO: The UI is a very simple functional UI designed just to test the Engine.
    //  I have chosen to concentrate on engine implementation and testing rather than
    //  adding an elegant UI.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.container, new MainFragment())
                    .commit();
        }
    }

    @Override
    public void onSchoolSelected(String dbn) {
        SchoolFragment newFragment = new SchoolFragment();
        Bundle args = new Bundle();
        args.putString(SchoolFragment.DBN, dbn);
        newFragment.setArguments(args);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
