package com.greatape.nycschools;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.greatape.engine.NYCSchoolEngine;
import com.greatape.engine.gson.NYCSchoolData;

import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;

public class SchoolFragment extends Fragment {
    private final static String TAG = "SchoolFragment";
    public final static String DBN = "SchoolDbn";

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.school_fragment, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        Bundle args = getArguments();
        if (args != null) {
            String dbn = args.getString(DBN);
            if (dbn != null) {
                queryByDbn(dbn);
            }
        }
    }

    private void queryByDbn(String dbn) {
        NYCSchoolEngine engine = NYCSchoolEngine.getInstance();
        engine.getSchoolData(dbn, new SingleObserver<NYCSchoolData>() {
            @Override
            public void onSubscribe(Disposable d) {}

            @Override
            public void onSuccess(NYCSchoolData schoolData) {
                setViewData(schoolData);
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "Error retrieving school list: " + e.toString());
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle(R.string.school_data_error);
                builder.setMessage(e.getMessage());
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
                View view = getView();
                if (view != null) {
                    setText(view, R.id.school_name, getString(R.string.school_data_not_found));
                }
            }
        });
    }

    private void setViewData(NYCSchoolData schoolData) {
        View view = getView();
        if (view != null) {
            setText(view, R.id.school_name, schoolData.school_name);
            setText(view, R.id.num_of_sat_test_takers, schoolData.num_of_sat_test_takers);
            setText(view, R.id.sat_critical_reading_avg_score, schoolData.sat_critical_reading_avg_score);
            setText(view, R.id.sat_math_avg_score, schoolData.sat_math_avg_score);
            setText(view, R.id.sat_writing_avg_score, schoolData.sat_writing_avg_score);
        }
    }

    private void setText(View view, int id, int value) {
        setText(view, id, String.valueOf(value));
    }

    private void setText(View view, int id, String text) {
        TextView textView = view.findViewById(id);
        textView.setText(text);
    }
}
