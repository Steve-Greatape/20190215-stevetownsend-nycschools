package com.greatape.engine.service;

import com.greatape.engine.gson.NYCSchool;
import com.greatape.engine.gson.NYCSchoolData;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NYCSchoolService {
    @GET("resource/s3k6-pzi2.json")
    Call<List<NYCSchool>> schoolList(@Query("$$app_token") String app_token,
                                     @Query("$order") String order,
                                     @Query("$limit") int limit,
                                     @Query("$offset") int offset);

    @GET("resource/f9bf-2cp4.json")
    Call<List<NYCSchoolData>> schoolData(@Query("$$app_token") String app_token, @Query("dbn") String dbn);
}
