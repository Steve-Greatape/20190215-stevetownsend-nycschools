package com.greatape.engine.gson;

public class NYCSchoolData {
    public String school_name;
    public String dbn;
    public int num_of_sat_test_takers;
    public int sat_critical_reading_avg_score;
    public int sat_math_avg_score;
    public int sat_writing_avg_score;
}
