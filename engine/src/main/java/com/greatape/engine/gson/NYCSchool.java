package com.greatape.engine.gson;

public class NYCSchool {
    // TODO: I have only specified a small subset of available data. This should be enough to
    //  properly implement and test the engine. Additional fields can simply be added as required.
    public String school_name;
    public String borough;
    public String dbn;
}
