package com.greatape.engine;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.greatape.engine.gson.NYCSchool;
import com.greatape.engine.gson.NYCSchoolData;
import com.greatape.engine.service.NYCSchoolService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleObserver;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NYCSchoolEngine {
    private static final String TAG = "NYCSchoolEngine";

    // TODO: A more comprehensive solution would make this configurable
    private final static String NYC_SCHOOL_URL = "https://data.cityofnewyork.us/";
    // TODO: Configurable ordering
    private final static String DEFAULT_ORDER = ":id";
    // TODO: Make page size configurable
    // For now I've chosen a number small enough to make sure we test the paging logic,
    // in production we would probably use a larger number
    private final int PAGE_SIZE = 50;

    private final NYCSchoolService mRetroFitService;
    private String mAppToken;

    private static NYCSchoolEngine sInstance;

    public static NYCSchoolEngine getInstance() {
        if (sInstance == null) {
            sInstance = new NYCSchoolEngine();
        }
        return sInstance;
    }

    private NYCSchoolEngine() {
        // The numeric fields can contain "s" instead of a valid number.
        // I have used registerTypeAdapter to add an int type adapter to handle the errors.
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(int.class, intTypeAdapter())
                .create();

        // TODO: Added for logging during development, production code will need to make sure unnecessary logging is not enabled.
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        mRetroFitService = new Retrofit.Builder()
                .baseUrl(NYC_SCHOOL_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build().create(NYCSchoolService.class);
    }

    public void setAppToken(String appToken) {
        mAppToken = appToken;
    }

    public void getSchoolList(final Observer<List<NYCSchool>> observer) {
        // TODO: Allow engine to selectively page through the list so we can avoid reading
        // more of the list than is required by the UI
        Observable<List<NYCSchool>> observable = Observable.create(new ObservableOnSubscribe<List<NYCSchool>>() {
            @Override
            public void subscribe(ObservableEmitter<List<NYCSchool>> emitter) throws Exception {
                int offset = 0;
                boolean finished = false;
                do {
                    Response<List<NYCSchool>> response = mRetroFitService.schoolList(mAppToken, DEFAULT_ORDER, PAGE_SIZE, offset).execute();
                    if (response.isSuccessful()) {
                        List<NYCSchool> schoolList = response.body();
                        if (schoolList != null) {
                            emitter.onNext(schoolList);
                            offset += schoolList.size();
                        }
                        if (schoolList == null || schoolList.size() < PAGE_SIZE) {
                            emitter.onComplete();
                            finished = true;
                        }
                    } else {
                        emitter.onError(new IOException(parseError(response)));
                        finished = true;
                    }
                } while(!finished);
            }
        })
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread());
        observable.subscribe(observer);
    }

    public void getSchoolData(final String dbn, SingleObserver<NYCSchoolData> observer) {
        Single<NYCSchoolData> observable = Single.create(new SingleOnSubscribe<NYCSchoolData>() {
            @Override
            public void subscribe(SingleEmitter<NYCSchoolData> emitter) throws Exception {
                Response<List<NYCSchoolData>> response = mRetroFitService.schoolData(mAppToken, dbn).execute();
                if (response.isSuccessful()) {
                    List<NYCSchoolData> list = response.body();
                    if (list == null || list.isEmpty()) {
                        emitter.onError(new IOException("No schools found for dbn=" + dbn));
                    } else {
                        if (list.size() > 1) {
                            Log.e(TAG, "More than on school returned for dbn=" + dbn);
                        }
                        emitter.onSuccess(list.get(0));
                    }
                } else {
                    emitter.onError(new IOException(parseError(response)));
                }
            }
        })
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread());
        observable.subscribe(observer);
    }

    private String parseError(Response response) throws IOException, JSONException {
        String error = null;
        ResponseBody errorBody = response.errorBody();
        if (errorBody != null) {
            JSONObject jsonError = new JSONObject(errorBody.string());
            error = jsonError.getString("message");
        }
        if (error == null || error.isEmpty()) {
            // TODO: Currently the engine has no access to resource strings. This is only an
            //  emergency fall back error message in case the response does not contain one. So
            //  we can skip localisation of this error message as it should never be seen.
            error = "Error " + response.code() + " retrieving school data.";
        }
        return error;
    }

    private Object intTypeAdapter() {
        return new JsonDeserializer<Integer>() {
            @Override
            public Integer deserialize(JsonElement json, Type type, JsonDeserializationContext jsonDeserializationContext){
                String jsonString = json.getAsJsonPrimitive().getAsString();
                int value;
                try {
                    value = Integer.parseInt(jsonString);
                } catch(NumberFormatException e) {
                    value = -1;
                }
                return value;
            }
        };
    }
}
