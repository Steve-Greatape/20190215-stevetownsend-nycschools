package com.greatape.engine;

import android.util.Log;

import com.greatape.engine.gson.NYCSchool;
import com.greatape.engine.gson.NYCSchoolData;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import io.reactivex.Observer;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class FetchAllSchoolDataTest extends SchoolTestBase {
    private final static String TAG = "FetchAllSchoolDataTest";
    private final static String ExpectedMissingDbnError = "No schools found for dbn=";

    @Test
    public void fetchAllSchoolData() {
        NYCSchoolEngine engine = NYCSchoolEngine.getInstance();
        engine.setAppToken(TestAppToken);
        final List<NYCSchool> schoolList = new ArrayList<>();
        final CountDownLatch latch = new CountDownLatch(1);
        engine.getSchoolList(new Observer<List<NYCSchool>>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onNext(List<NYCSchool> nycSchools) {
                schoolList.addAll(nycSchools);
            }

            @Override
            public void onError(Throwable e) {
                fail("getSchoolList error=" + e.toString());
            }

            @Override
            public void onComplete() {
                latch.countDown();
            }
        });
        try {
            latch.await();
            verifySchoolData(schoolList);
        } catch (InterruptedException e) {
            fail("Latch wait failed with: " + e.toString());
        }
    }

    private void verifySchoolData(List<NYCSchool> schoolList) {
        for(NYCSchool school : schoolList) {
            verifySchoolData(school);
        }
    }

    private void verifySchoolData(NYCSchool school) {
        NYCSchoolEngine engine = NYCSchoolEngine.getInstance();
        engine.setAppToken(TestAppToken);
        final CountDownLatch latch = new CountDownLatch(1);
        final NYCSchoolData[] schoolArray = new NYCSchoolData[1];
        final String errorArray[] = new String[1];
        engine.getSchoolData(school.dbn, new SingleObserver<NYCSchoolData>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onSuccess(NYCSchoolData nycSchoolData) {
                schoolArray[0] = nycSchoolData;
                latch.countDown();
            }

            @Override
            public void onError(Throwable e) {
                errorArray[0] = e.getMessage();
                latch.countDown();
            }
        });
        try {
            latch.await();
            NYCSchoolData schoolData = schoolArray[0];
            if (schoolData != null) {
                // TODO: The verification of data here is simply done so it passes on the current
                //  existing data set. Ideally we need to understand the full range of possible
                //  responses and handle all of them properly.
                assertNotNull(schoolData);
                assertNotNull(schoolData.dbn);
                assertFalse(schoolData.dbn.isEmpty());
                if (schoolData.num_of_sat_test_takers == -1) {
                    // Some schools have all numeric fields set to "s". The engine converts those to -1
                    Log.d(TAG, "Missing data for school: " + school.school_name + " dbn=" + school.dbn);
                    assertEquals(-1, schoolData.sat_critical_reading_avg_score);
                    assertEquals(-1, schoolData.sat_math_avg_score);
                    assertEquals(-1, schoolData.sat_writing_avg_score);
                } else {
                    assertTrue(schoolData.num_of_sat_test_takers >= 0);
                    assertTrue(satScoreIsValid(schoolData.sat_critical_reading_avg_score));
                    assertTrue(satScoreIsValid(schoolData.sat_math_avg_score));
                    assertTrue(satScoreIsValid(schoolData.sat_writing_avg_score));
                    Log.d(TAG, "Verified data for school: " + school.school_name + " dbn=" + school.dbn);
                }
            } else {
                // A lot of the schools in the list have missing data, so check we get the expected error message for those.
                Log.d(TAG, "Missing data for school: " + school.school_name + " dbn=" + school.dbn);
                String error = errorArray[0];
                assertNotNull(error);
                assertEquals(ExpectedMissingDbnError + school.dbn, error);
            }
        } catch (InterruptedException e) {
            fail("Latch wait failed with: " + e.toString());
        }
    }
}
