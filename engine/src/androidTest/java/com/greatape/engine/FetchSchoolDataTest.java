package com.greatape.engine;

import android.support.test.runner.AndroidJUnit4;

import com.greatape.engine.gson.NYCSchool;
import com.greatape.engine.gson.NYCSchoolData;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.concurrent.CountDownLatch;

import io.reactivex.Observer;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class FetchSchoolDataTest extends SchoolTestBase {
    private final static String TestDbnArray[] = {"09X505", "11X513", "21K728"};

    @Test
    public void fetchSchoolData() {
        for(String dbn : TestDbnArray) {
            doFetchSchoolData(dbn);
        }
    }

    private void doFetchSchoolData(String dbn) {
        NYCSchoolEngine engine = NYCSchoolEngine.getInstance();
        engine.setAppToken(TestAppToken);
        final CountDownLatch latch = new CountDownLatch(1);
        final NYCSchoolData[] schoolArray = new NYCSchoolData[1];
        engine.getSchoolData(dbn, new SingleObserver<NYCSchoolData>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onSuccess(NYCSchoolData nycSchoolData) {
                schoolArray[0] = nycSchoolData;
                latch.countDown();
            }

            @Override
            public void onError(Throwable e) {
                fail("getSchoolList error=" + e.toString());
            }
        });
        try {
            latch.await();
            NYCSchoolData schoolData = schoolArray[0];
            assertNotNull(schoolData);
            assertNotNull(schoolData.dbn);
            assertFalse(schoolData.dbn.isEmpty());
            assertTrue(schoolData.num_of_sat_test_takers >= 0);
            assertTrue(satScoreIsValid(schoolData.sat_critical_reading_avg_score));
            assertTrue(satScoreIsValid(schoolData.sat_math_avg_score));
            assertTrue(satScoreIsValid(schoolData.sat_writing_avg_score));
        } catch (InterruptedException e) {
            fail("Latch wait failed with: " + e.toString());
        }
    }

    @Test
    public void errorFetchingSchoolData() {
        NYCSchoolEngine engine = NYCSchoolEngine.getInstance();
        engine.setAppToken(InvalidAppToken);
        final CountDownLatch latch = new CountDownLatch(1);
        final String[] errorArray = new String[1];
        engine.getSchoolData(TestDbnArray[0], new SingleObserver<NYCSchoolData>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onSuccess(NYCSchoolData nycSchoolData) {
                fail("Unexpected onNext() with invalid app key");
            }

            @Override
            public void onError(Throwable e) {
                errorArray[0] = e.getMessage();
                latch.countDown();
            }
        });
        try {
            latch.await();
            String error =  errorArray[0];
            assertNotNull(error);
            assertEquals(ExpectedInvalidAppTokenError, error);
        } catch (InterruptedException e) {
            fail("Latch wait failed with: " + e.toString());
        }
    }
}
