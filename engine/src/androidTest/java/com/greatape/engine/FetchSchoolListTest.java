package com.greatape.engine;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.greatape.engine.gson.NYCSchool;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class FetchSchoolListTest extends SchoolTestBase {
    // TODO: Choose realistic criteria to check retrieved list looks genuine
    private final static int MinimumExpectedListSize = 100;

    @Test
    public void fetchSchoolList() {
        NYCSchoolEngine engine = NYCSchoolEngine.getInstance();
        engine.setAppToken(TestAppToken);
        final List<NYCSchool> schoolList = new ArrayList<>();
        final CountDownLatch latch = new CountDownLatch(1);
        engine.getSchoolList(new Observer<List<NYCSchool>>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onNext(List<NYCSchool> nycSchools) {
                schoolList.addAll(nycSchools);
            }

            @Override
            public void onError(Throwable e) {
                fail("getSchoolList error=" + e.toString());
            }

            @Override
            public void onComplete() {
                latch.countDown();
            }
        });
        try {
            latch.await();
            verifySchoolList(schoolList);
        } catch (InterruptedException e) {
            fail("Latch wait failed with: " + e.toString());
        }
    }

    private void verifySchoolList(List<NYCSchool> schoolList) {
        assertTrue(schoolList.size() >= MinimumExpectedListSize);
        for(NYCSchool school : schoolList) {
            assertNotNull(school.school_name);
            assertFalse(school.school_name.isEmpty());
            // TODO: This test fails as some schools are returned with a null value for borough
//            assertNotNull(school.borough);
//            assertFalse(school.borough.isEmpty());
            assertNotNull(school.dbn);
            assertFalse(school.dbn.isEmpty());
        }
    }

    @Test
    public void errorFetchingSchoolList() {
        NYCSchoolEngine engine = NYCSchoolEngine.getInstance();
        engine.setAppToken(InvalidAppToken);
        final CountDownLatch latch = new CountDownLatch(1);
        final String[] errorArray = new String[1];
        engine.getSchoolList(new Observer<List<NYCSchool>>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onNext(List<NYCSchool> nycSchools) {
                fail("Unexpected onNext() with invalid app key");
            }

            @Override
            public void onError(Throwable e) {
                errorArray[0] = e.getMessage();
                latch.countDown();
            }

            @Override
            public void onComplete() {
                fail("Unexpected onComplete() invalid app key");
            }
        });
        try {
            latch.await();
            String error =  errorArray[0];
            assertNotNull(error);
            assertEquals(ExpectedInvalidAppTokenError, error);
        } catch (InterruptedException e) {
            fail("Latch wait failed with: " + e.toString());
        }
    }
}
