package com.greatape.engine;

abstract class SchoolTestBase {
    // TODO: If we are going to open source this code this token would need to loaded
    // from a configuration file that could be excluded from the public repo.
    final static String TestAppToken = "PYC8Nwe7gQ3DaBPPY8G5tL51O";
    final static String InvalidAppToken = "ZZZ";
    final static String ExpectedInvalidAppTokenError = "Invalid app_token specified";

    // TODO: Find out value range for max min scores
    private final static int MIN_SAT_SCORE = 0;
    private final static int MAX_SAT_SCORE = 1000;

    boolean satScoreIsValid(int satScore) {
        return satScore >= MIN_SAT_SCORE && satScore < MAX_SAT_SCORE;
    }
}
